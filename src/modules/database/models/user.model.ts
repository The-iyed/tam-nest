import mongoose from 'mongoose';
import * as bcrypt from 'bcryptjs';

const userSchema = new mongoose.Schema(
   {
      id:mongoose.Schema.Types.ObjectId,
      firstName:{Type:String,required:true,maxLength:20},
      lastName:{Type:String,required:true,maxLength:20},
      email:{Type:String,required:true,unique:true,maxLength:200},
      age:{Type:Number,required:false},
      isVerified:{type:Boolean,required:true,defautl:false},
      deletedAt:{Type:Date},
      updatedAt:{Type:Date},
      createdAt:{Type:Date}
   },{
      timestamps:true
   }
)